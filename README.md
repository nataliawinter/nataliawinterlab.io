# My first blog using the [Cookie Theme](https://github.com/abhinavs/cookie).

Cookie is a Jekyll and Tailwind CSS based static website that makes the whole process of creating and launching landing websites extremely easy. With its responsive and mobile friendly pages, integrated blog, additional pages and [Soopr](https://www.soopr.co) integration, you can 
focus on building your product than landing website.

## Installation
`bin/bootstrap`

## Starting Server
`bin/start` - development server will start at http://127.0.0.1:4061

## Acknowledgement
Cookie uses landing page provided by [Tailwind Starter Kit](https://www.creative-tim.com/learning-lab/tailwind-starter-kit/presentation) - thanks for providing an amazing landing page under MIT License. Initial code was also inspired by [Jekyll TailwindUI](https://github.com/chunlea/jekyll-tailwindui)

## License
This project is available as open source under the terms of the [MIT License](https://opensource.org/licenses/MIT).

✨⚡You can read more about me on my [blog](https://www.abhinav.co/about/) or follow me on Twitter - [@winternati](https://twitter.com/winternati)